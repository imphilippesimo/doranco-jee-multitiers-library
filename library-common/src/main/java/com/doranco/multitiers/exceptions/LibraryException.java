package com.doranco.multitiers.exceptions;

public class LibraryException extends Exception {

	public LibraryException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LibraryException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public LibraryException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public LibraryException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public LibraryException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	

}
