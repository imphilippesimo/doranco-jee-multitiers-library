package com.doranco.multitiers.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.doranco.multitiers.utils.Page;

public class GenericDAO<T> {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("library");
	EntityManager em = emf.createEntityManager();
	public EntityTransaction transaction = em.getTransaction();

	private String jpql = null;

	public T create(T toBeCreated) throws Exception {

		transaction.begin();
		em.persist(toBeCreated);
		transaction.commit();
		return toBeCreated;

	}

	public void delete(T toBeDeleted) throws Exception {
		transaction.begin();
		em.remove(toBeDeleted);
		transaction.commit();
	}

	public T find(Class clazz, Integer primaryKey) throws Exception {
		return (T) em.find(clazz, primaryKey);
	}

	public T update(T toBeUpdated) throws Exception {

		transaction.begin();
		em.merge(toBeUpdated);
		transaction.commit();
		return toBeUpdated;
	}

	public Page<T> find(Class clazz, int pageNumber, int pageSize) throws Exception {

		// récupération du nom de l'entité
		String className = getClassName(clazz);

		// récupération d'une page de données de l'entités
		jpql = "SELECT c FROM" + className + " c";
		Query query = em.createQuery(jpql);
		query.setFirstResult((pageNumber - 1) * pageSize);
		query.setMaxResults(pageSize);

		// récupération du nombre total de données
		Query queryTotal = em.createQuery("Select count(c.id) from " + className + " c");
		long totalCount = (long) queryTotal.getSingleResult();

		// construction de la page à retourner
		Page<T> page  = new Page<T>();
		
		page.setTotalCount(totalCount);
		page.setContent(query.getResultList());

		return page;

	}

	private String getClassName(Class clazz) {
		String fqName = clazz.getName();
		int firstCharOffset = fqName.lastIndexOf(".") + 1;
		if (firstCharOffset > 0)
			fqName = fqName.substring(firstCharOffset);
		return fqName;
	}

}
