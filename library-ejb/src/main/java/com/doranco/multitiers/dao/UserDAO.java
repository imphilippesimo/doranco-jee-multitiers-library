package com.doranco.multitiers.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import com.doranco.multitiers.entity.User;

@Stateless
@LocalBean
public class UserDAO extends GenericDAO<User> {

	String jpql;

	public User findByUserNameAndPassword(String givenUserName, String givenPassword) throws NoResultException {

		jpql = "SELECT u FROM User u WHERE u.userName= :userName AND u.password= :password ";
		return (User) em.createQuery(jpql).setParameter("userName", givenUserName)
				.setParameter("password", givenPassword).getSingleResult();
	}

}
