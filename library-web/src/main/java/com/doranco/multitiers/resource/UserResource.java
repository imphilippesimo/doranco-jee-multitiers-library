package com.doranco.multitiers.resource;

import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.apache.log4j.Logger;

import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.IUser;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;

@Path("users")
public class UserResource {

	IUser iUser = null;

	Logger logger = Logger.getLogger(UserResource.class);

	
	public UserResource() throws LibraryException {
		super();
		try {
			iUser = (IUser) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/UserBean");
		} catch (NamingException e) {
			logger.error("Imposible la ressource UserResource", e);
			throw new LibraryException();
		}

	}

	@POST
	public User suscribe(User user) {
		
		try {
			user = iUser.suscribe(user);
		} catch (LibraryException e) {
			logger.error("Impossible de démarrer la ressource UserResource", e);
			//TODO Send a custom message to the user
		}
		
		return user;

	}
	
	@GET
	public Page<User> getUsers(@QueryParam("pageSize") int pageSize, @QueryParam("pageNumber") int pageNumber){
		
		Page<User> page =null;
		try {
			page = iUser.getUsers(pageNumber, pageSize);
			
		} catch (Exception e) {
			logger.error("Problème lors d ela récupération des users", e);
			//TODO Send a custom message to the user
		}
		return page;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
